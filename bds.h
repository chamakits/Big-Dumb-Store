#ifndef BDS_HEADER
#define BDS_HEADER
#define NUMBER_DIGITS 3
#define SYSTEM_HOME "/home/chamakits/.config/big-dumb-store/.store_reverse"
#define STORE_HOME "/home/chamakits/.config/big-dumb-store/"
#define DEFAULT_STORE ".v6_store"
#define FILE_NAME_BYTE_AMOUNT 256*4
#include <unistd.h>

struct key_value
{
    char *file_name;
    char *value;
    char *key;
    char *key_size;
    int key_char_amount;
    char *value_size;
    int value_char_amount;
    int print_result;
};

struct server_info
{
    int in_server_mode;
    
};

void create_kv_file_if_not_exists(struct key_value* kv);
void prepare_kv(char*, struct key_value*);
void read_in(struct key_value*);
void store_put(char*, struct key_value*);
void store_get(char*, struct key_value*);
void to_num_str(char* , int , int );
void create_file_and_or_directory(struct key_value* kv);
#endif
