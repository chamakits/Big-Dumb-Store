#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include "bds.h"

#define RECV_BUFF_SIZE 2050

#define PORT "3490"  // the port users will be connecting to

#define BACKLOG 10	 // how many pending connections queue will hold

void sigchld_handler(int s)
{
	while(waitpid(-1, NULL, WNOHANG) > 0);
}

void *get_in_addr(struct sockaddr *sa)
{
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}



int init_server(struct addrinfo *setup_in, struct addrinfo *list, struct addrinfo *current)
{
    int rv, sockfd;
    int yes=1;
    struct addrinfo setup = *setup_in;
    memset(&setup, 0, sizeof setup);
    /*
    (*setup).ai_family = AF_UNSPEC;
    (*setup).ai_socktype = SOCK_STREAM;
    (*setup).ai_flags = AI_PASSIVE;
    */

    setup.ai_family = AF_UNSPEC;
    setup.ai_socktype = SOCK_STREAM;
    setup.ai_flags = AI_PASSIVE;

    if ((rv = getaddrinfo(NULL, PORT, &setup, &list)) != 0) 
    {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		exit(1);
    }
    
    for(current = list; current != NULL; current = current->ai_next) 
    {
	if ((sockfd = socket(current->ai_family, current->ai_socktype,
			     current->ai_protocol)) == -1) 
	{
	    perror("server: socket");
	    continue;
	}
	
	if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes,
		       sizeof(int)) == -1) 
	{
	    perror("setsockopt");
	    exit(1);
	}

	if (bind(sockfd, current->ai_addr, current->ai_addrlen) == -1) 
	{
	    close(sockfd);
	    perror("server: bind");
			continue;
	}
	
	break;
    }

    if (current == NULL)  
    {
	fprintf(stderr, "server: failed to bind\n");
	return 2;
    }

    freeaddrinfo(list);

    return sockfd;


}

void serv_loop(int sockfd, struct sockaddr_storage their_addr)
{
    int sin_size=0, new_fd=0;
    char s[INET6_ADDRSTRLEN];
    //char recv_buffer[1024*2 +1]={'\0'};
    char *recv_buffer;
    char *key;
    char *value;

    while(1) // main accept() loop
    {
	sin_size = sizeof (their_addr);
	new_fd = accept(sockfd, (struct sockaddr *) &their_addr, &sin_size);
	if (new_fd == -1) {
	    perror("accept");
	    continue;
	}

	inet_ntop((their_addr).ss_family,
		  get_in_addr((struct sockaddr *) &their_addr),
		  s, sizeof s);
	printf("server: got connection from %s\n", s);
	
	printf("Pre-fork\n");
	if (!fork()) 
	{ // this is the child process
	    printf("In-fork\n");
	    recv_buffer=malloc(sizeof(char)*RECV_BUFF_SIZE);
	    memset(recv_buffer,0,sizeof (char)*RECV_BUFF_SIZE);
	    printf("Receiving!\n");
	    recv(sockfd,recv_buffer,RECV_BUFF_SIZE,0);
	    
	    key=recv_buffer;
	    while(*(recv_buffer++)!='@')printf("Skipping:%c\n",*recv_buffer);
	    value=recv_buffer;
	    --recv_buffer;
	    (*recv_buffer)='\0';
	    printf("Key:%s\n",key);
	    printf("Value:%s\n",value);
	    
	    close(sockfd); // child doesn't need the listener
	    if (send(new_fd, "Hello, world!", 13, 0) == -1)
				perror("send");
	    close(new_fd);
	    exit(0);
	}
	close(new_fd);  // parent doesn't need this
    }
}

void server_receive(struct server_info *serv_i)
{
 
    int sockfd, accepted_fd;
    struct addrinfo setup, *list, *current;
    struct sockaddr_storage client_addr;
    socklen_t client_s;
    struct sigaction sa;
    char s[INET6_ADDRSTRLEN];
    sockfd = init_server(&setup, list, current);
    
    if (listen(sockfd, BACKLOG) == -1) 
    {
	perror("listen");
	exit(1);
    }
    
    sa.sa_handler = sigchld_handler; // reap all dead processes
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    if (sigaction(SIGCHLD, &sa, NULL) == -1) 
    {
	perror("sigaction");
	exit(1);
    }
    serv_loop(sockfd, client_addr);
}
