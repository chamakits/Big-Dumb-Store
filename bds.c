#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#define SYSTEM_HOME "/home/chamakits/.config/big-dumb-store/.store_reverse"
#define STORE_HOME "/home/chamakits/.config/big-dumb-store/"
#define DEFAULT_STORE ".v6_store"
#define FILE_NAME_BYTE_AMOUNT 256*4

#include "bds.h"

void create_kv_file_if_not_exists(struct key_value* kv)
{

  struct stat file_stat;
  //Doesn't exists
  if(!stat(kv->file_name,&file_stat))
    {
      //printf("Inside if.\n");
      //Verify if the directory exists, and if it doesn't, create the directory, then the file.
      if( file_stat.st_mode && 
	  (S_ISDIR(file_stat.st_mode) || S_ISREG(file_stat.st_mode))
	  )
        {
	  //printf("File does exist.\n");   
        }
      else
        {
	  create_file_and_or_directory(kv);
	  //printf("File does NOT exist.\n");   
        }

    }
  else
    {
      //perror("stat() error\n");
      create_file_and_or_directory(kv);
    }


}

void create_file_and_or_directory(struct key_value* kv)
{
  //mkdir(kv->file_name);
  FILE *file = fopen(kv->file_name, "a");
  if(file)
    {
      //printf("Preclose\n");
      fclose(file);
      //printf("Postclose\n");
    }
}


void prepare_kv(char* filename, struct key_value* kv)
{
  if(filename==NULL||strlen(filename)<1)
    {
      //printf("Filename is null.\n");
      filename=(malloc(sizeof(char)*FILE_NAME_BYTE_AMOUNT));
      memcpy(filename,DEFAULT_STORE,strlen(DEFAULT_STORE));
      //printf("%s",filename);
    }
  //printf("Post if.\n");
  //printf("filename:%s\n",filename);
  size_t len1=strlen(STORE_HOME);
  size_t len2=strlen(filename);

  char *s = malloc(len1+len2+2);
  memcpy(s,STORE_HOME,len1);
  //s[len1]=' ';
  memcpy(s+len1,filename, len2+1);
  kv->file_name=s;
  //printf("DEBUG:prepare:%s\n",kv->file_name);
}

void read_in(struct key_value* kv)
{
  kv->value=malloc(sizeof(char)*1024);
  scanf("%999c",kv->value);
}

void store_put(char* key, struct key_value* kv)
{
  char key_size[8]={[0 ... 7] = '\0'};
  kv->key_size=key_size;
  char value_size[8]={[0 ... 7] = '\0'};
  //Keep adding to buffer as long as not EOF returned.
  kv->value_size=value_size;

  int in_read; 
  in_read=strlen(kv->value);
    
  //put the size of the value into a string
  to_num_str(value_size, in_read, NUMBER_DIGITS);
    
  in_read=strlen(key);
  to_num_str(key_size, in_read, NUMBER_DIGITS);
    

  FILE *bds_file= fopen(kv->file_name,"a");
  if(bds_file == NULL)
    {
      printf("Failed to open key file.");
    }
  else
    {
      //printf("Key: %s keysize: %s, Value:%s, valuesize:%s\n",kv->value, key_size, kv->value, value_size);
      fprintf(bds_file,"%s%s%s%s",kv->value,key,value_size,key_size);
      fclose(bds_file);
    }

}
void store_get(char* key_arg, struct key_value* kv)
{
  kv->print_result=1;
  char value[1024]={'\0'};
  kv->value=value;
  char key[1024]={'\0'};
  kv->key = key;
  char key_size[8]={[0 ... 7] = '\0'};
  kv->key_size=key_size;
  char value_size[8]={[0 ... 7] = '\0'};
  kv->value_size=value_size;

  int scan_result=0;
  int comp_result=0;


  char tempbuff[8]={'\0'};

  FILE *bds_file= fopen(kv->file_name,"r");

  fseek(bds_file, 0, SEEK_END);
  do
    {
	
      fseek(bds_file, -3, SEEK_CUR);	
      //scan_result=fscanf(bds_file,"%3c",key_size);
      scan_result=ftell(bds_file);
	
      //printf("DEBUG:scan_result=%d\tkey_size=%s\n",scan_result,key_size);
      if(scan_result ==0 || scan_result == EOF)
	{
	  break;
	}

      fscanf(bds_file,"%3c",key_size);

      kv->key_char_amount=atoi(key_size);
	
      fseek(bds_file, -6, SEEK_CUR);
      fscanf(bds_file,"%3c",value_size);
	
      kv->value_char_amount=atoi(value_size);
	
      fseek(bds_file,-(kv->key_char_amount+3),SEEK_CUR);
      fgets(key,kv->key_char_amount+1,bds_file);
	
      comp_result=strcmp(key,key_arg);

      fseek(bds_file,-(kv->value_char_amount+kv->key_char_amount),SEEK_CUR);

      if(!comp_result)
	{
	  fgets(value,(kv->value_char_amount+1),bds_file);
	}
	

    }while(comp_result);
    
  if(comp_result)
    {
      kv->value="ERR:Could not find value for key.";
    }
  fclose(bds_file);

}

void to_num_str(char* str, int number, int amount)
{
  int base_mult=1;
  int mult = 10;
  char curr_char;
  amount-=1;

  while(amount >=0)
    {
      curr_char=((number/base_mult)%mult+'0');
      str[amount--]=curr_char;
      base_mult*=mult;
    }
}
