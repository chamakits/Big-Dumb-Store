#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "bds.h"

//TODO:  If you are getting a segmentation fault, its VERY possible that the file isn't created, nor the directory.

/*
  TODO:
  1)Make server.
  2)Make size of key and value part of the data 
  (have one character store the amount of digits the key size
  and the value size take up), and make it up to nine characters
  big each. (possibly more with hexadecimal numbers).
  3)Make a serializable type (have a special character for keys
  like '@', to identify certain information after.  You can identify
  that the next 'n' keys/value pairs are serializable and part of 
  the same key.)

*/

int main(int argc, char* argv[])
{
  //struct key_value* kv = ();
  struct key_value* kv = 
    (struct key_value*) 
    malloc(sizeof (struct key_value));

  //    printf("DEBUG: Pre prepare\n");

  struct server_info *serv_i =
    (struct server_info*)
    malloc(sizeof (struct server_info));

  (*serv_i).in_server_mode=0;
  if(argc>2)
    {
      do
	{
	  //Assumes that the first flag has the file with the store name appeneded to it.
	  //Example "./bds.out p~/.configure/keystore.txt MY_KEY"
	  kv->file_name=(argv[1]+1);
	  //printf("kv->file_name:%s\n",kv->file_name);
	  //printf("argv[1]:%s\n",argv[1]);
	  prepare_kv(kv->file_name,kv);
	  create_kv_file_if_not_exists(kv);
	
	  switch(argv[1][0])
	    {
	    case 'p':
	      read_in(kv);
	      //printf("Post read\n");
	      store_put(argv[2],kv);
	      break;
	    case 'g':
	      store_get(argv[2],kv);
	      break;
	    case 's':
	      server_receive(serv_i);
	    }
	}
      while((*serv_i).in_server_mode);
    }
  else
    {
      fprintf(stderr,"No flags given.  Clearly you've never used the program... head.\n\nAnyways, the flags are:\np\ng\ns\n\n\n");
      exit(1);
    }

  if(kv->print_result)
    {
      fprintf(stdout,"%s",kv->value);
    }

}
